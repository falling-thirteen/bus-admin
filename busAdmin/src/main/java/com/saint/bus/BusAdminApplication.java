package com.saint.bus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SpringBoot启动类
 *
 * @author Saint
 * @date 2022/09/03
 */
@SpringBootApplication
public class BusAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(BusAdminApplication.class, args);
    }

}
