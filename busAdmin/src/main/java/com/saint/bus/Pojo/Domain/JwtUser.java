package com.saint.bus.Pojo.Domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.saint.bus.Pojo.Entity.Role;
import com.saint.bus.Pojo.Entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Saint
 * @description JWT用户对象
 */
@Component
@Data
@AllArgsConstructor
public class JwtUser implements UserDetails {

    @JsonIgnore
    @Resource
    IRoleService iRoleService;
    private String userId;
    private Integer roleId;
    private String userName;
    private String telephone;
    private Integer age;
    @JsonIgnore
    private String password;
    private String email;
    private String headimg;
    private String token;
    private String describtion;
    private LocalDate birthday;
    private Boolean enabled;
    private Integer role;
    private Integer sex;
    private Collection<? extends GrantedAuthority> authorities;

    /**
     * jwt用户
     */
    public JwtUser() {
    }

    /**
     * jwt用户
     * 通过 user 对象创建jwtUser
     *
     * @param user 用户
     */
    public JwtUser(User user) {
        /**
         *
         */
        this.userId = user.getId();
        this.roleId = user.getRoleId();
        this.userName = user.getUserName();
        this.telephone = user.getPhoneNum();
        this.sex = user.getSex();
        this.password = user.getPassword();
        this.email = user.getEmail();
        this.headimg = user.getHeadImg();
        this.birthday = user.getBirthday();
        this.age = LocalDate.now().getYear() - user.getBirthday().getYear();
        enabled = true;
        role = user.getRoleId();
        authorities = this.getRoles();
    }

    private static JwtUser jwtUser;


    /**
     * 初始化
     * 在非controller和service中注入service和mapper需要下注解
     */
    @PostConstruct
    public void init() {
        jwtUser = this;
        jwtUser.iRoleService = this.iRoleService;
    }

    /**
     * 得到角色
     *
     * @return {@link List}<{@link SimpleGrantedAuthority}>
     */
    public List<SimpleGrantedAuthority> getRoles() {
        Role roleById = jwtUser.iRoleService.getById(role);
        String role = roleById.getName();
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_" + role));
        return authorities;
    }

    /**
     * 得到当局
     *
     * @return {@link Collection}<{@link ?} {@link extends} {@link GrantedAuthority}>
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    /**
     * 得到密码
     *
     * @return {@link String}
     */
    @Override
    public String getPassword() {
        return password;
    }

    /**
     * 获得用户名
     *
     * @return {@link String}
     */
    @Override
    public String getUsername() {
        return userName;
    }

    /**
     * 是账户非过期
     *
     * @return boolean
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * 是账户非锁定
     *
     * @return boolean
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * 是凭证不过期
     *
     * @return boolean
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * 启用了
     *
     * @return boolean
     */
    @Override
    public boolean isEnabled() {
        return this.enabled;
    }
}
