package com.saint.bus.Pojo.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDate;

/**
 * 用户
 *
 * @author Saint
 * @date 2022/09/07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class User {

    String id;

    Integer roleId;

    String headImg;

    LocalDate birthday;

    String userName;

    String phoneNum;

    Integer sex;

    String password;

    String email;


}
